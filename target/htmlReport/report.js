$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 2,
  "name": "Login Feature",
  "description": "Description: test login features with multiple scenario",
  "id": "login-feature",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@UserManagement"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "user login with valid credentials",
  "description": "",
  "id": "login-feature;user-login-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@SmokeTest"
    },
    {
      "line": 5,
      "name": "@RegressionTest"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "User is on Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "clicks on Login link tab provided on home page",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user enters valid username and password",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ],
      "line": 10
    },
    {
      "cells": [
        "saurabh1",
        "saurabh1"
      ],
      "line": 11
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "user login to application successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_is_on_Home_Page()"
});
formatter.result({
  "duration": 10290325700,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.clicks_on_Login_link_tab_provided_on_home_page()"
});
formatter.result({
  "duration": 110322200,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_enters_valid_username_and_password(DataTable)"
});
formatter.result({
  "duration": 577031600,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_login_to_application_successfully()"
});
formatter.result({
  "duration": 171600,
  "status": "passed"
});
});