@ProductManagement
Feature: Place Order - Laptop
Description: Test if user can place order for Laptop

Scenario Outline: User place an order for Laptop. 
Given User is on Home Page
And clicks on Laptop tab 
When user selects first Laptop 
And clicks on add to cart button
And clicks on ok button provided on alert pop up
And user moves to checkout from cart page
When user enters Name "<Name>" , Country "<Country>" , City "<City>" , CreditCard "<CreditCard>" , Month "<Month>" and Year "<Year>"
And Purchase the order
Then user receives thank you message on checkout page

Examples:
|Name|Country|City|CreditCard|Month|Year|
|saurabh1|India|Noida|123456789|July|2020|
