Feature: Place Order - Monitor
Description: Test if user can place order for Monitor

Scenario Outline: User place an order for Monitor 
Given User is on Home Page
And clicks on Monitor tab 
When user selects first Monitor 
And clicks on add to cart button
And clicks on ok button provided on alert pop up
And user moves to checkout from cart page
And user enters Name "<Name>" , Country "<Country>" , City "<City>" , CreditCard "<CreditCard>" , Month "<Month>" and Year "<Year>"
And Purchase the order
Then user receives thank you message on checkout page

Examples:
|Name|Country|City|CreditCard|Month|Year|
|saurabh|India|Noida|123456789|July|2020|
