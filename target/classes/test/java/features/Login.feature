@UserManagement
Feature: Login Feature
Description: test login features with multiple scenario

@SmokeTest @RegressionTest
Scenario: user login with valid credentials
Given User is on Home Page
And   clicks on Login link tab provided on home page 
When  user enters valid username and password
      |username|password|
      |saurabh1|saurabh1| 
Then  user login to application successfully

@RegressionTest
Scenario: user login with invalid credentials 
Given User is on Home Page
And   clicks on Login link tab provided on home page 
When  user enters invalid username and invalid password
      |saurabh|saurabh| 
Then  validaion for valid credentials appars on screen

@RegressionTest
Scenario: user login with no credentials 
Given User is on Home Page
And   clicks on Login link tab provided on home page 
When  user enters null username and null password
      ||| 
Then  validaion for valid credentials appars on screen

@RegressionTest
Scenario: user login with valid username and invalid password credentials 
Given User is on Home Page
And   clicks on Login link tab provided on home page 
When  user enters valid username and invalid password
      |saurabh1|saurabh| 
Then  validaion for valid credentials appars on screen

@RegressionTest
Scenario: user login with invalid username and valid password credentials 
Given User is on Home Page
And   clicks on Login link tab provided on home page 
When  user enters invalid username and valid password
      |saurabh|saurabh1| 
Then  validaion for valid credentials appars on screen