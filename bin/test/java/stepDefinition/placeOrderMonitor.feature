Feature: Place Order - Monitor
Description: Test if user can place order for Monitor

Scenario: User place an order for Monitor 
Given User is on Home Page
And clicks on Monitor tab 
When user selects first Monitor 
And clicks on add to cart button
And clicks on ok button provided on alert pop up
Then item is added to cart 
And user moves to checkout from cart page
And user enters Name "<Name>" , Country "<Country>" , City "<City>" , CreditCard "<CreditCard>" , Month "<Month>" and Year "<Year>"
And Place the order 


