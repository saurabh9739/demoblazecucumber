Feature: Place Order - Laptop
Description: Test if user can place order for Laptop

Scenario: User place an order for Laptop. 
Given User is on Home Page
And clicks on laptop tab 
When user selects first laptop 
And clicks on add to cart button
And clicks on ok button provided on alert pop up
Then item is added to cart 
And user moves to checkout from cart page
And user enters Name "<Name>" , Country "<Country>" , City "<City>" , CreditCard "<CreditCard>" , Month "<Month>" and Year "<Year>"
And Place the order 


