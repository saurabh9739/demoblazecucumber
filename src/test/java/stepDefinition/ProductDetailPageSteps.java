package test.java.stepDefinition;

import cucumber.api.java.en.When;
import main.java.context.TestContext;
import main.java.pageObjects.ProductDetailPage;

public class ProductDetailPageSteps {
	
	TestContext testContext;
	ProductDetailPage productDetailPage;
	
	public ProductDetailPageSteps(TestContext testContext)
	{
		this.testContext = testContext;
		productDetailPage = testContext.getPageObjectManagerInstance().getProductDetailPageInstance();
	}
	
	@When("^clicks on add to cart button$")
	public void clicks_on_add_to_cart_button() throws Throwable {		
		productDetailPage.clickOnAddToCartButtonOnProductDetailPage();
	}

	@When("^clicks on ok button provided on alert pop up$")
	public void clicks_on_ok_button_provided_on_alert_pop_up() throws Throwable {
		productDetailPage.productAddedAlert();
	}

}
