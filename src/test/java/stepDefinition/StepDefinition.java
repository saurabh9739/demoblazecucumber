/*package test.java.stepDefinition;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.java.managers.WebDriverManager;
import main.java.pageObjects.CartPage;
import main.java.pageObjects.CheckoutPage;
import main.java.pageObjects.HomePage;
import main.java.pageObjects.ProductDetailPage;

public class StepDefinition {
	WebDriverManager webDriverManager;
	WebDriver driver;
	HomePage homePage;
	ProductDetailPage productDetailPage;
	CartPage cartPage;
	CheckoutPage checkoutPage;
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		webDriverManager = new WebDriverManager();
		driver = webDriverManager.getDriver();
		homePage = new HomePage(driver);
		homePage.navigateToHomePage();
	}

	@Given("^clicks on Laptop tab$")
	public void clicks_on_Laptop_tab() throws Throwable {
		homePage.clickLaptopLinkOnHomePage();
	}

	@When("^user selects first Laptop$")
	public void user_selects_first_Laptop() throws Throwable {
	    homePage.clickOnFirstLaptop();
	}

	@When("^clicks on add to cart button$")
	public void clicks_on_add_to_cart_button() throws Throwable {
		productDetailPage = new ProductDetailPage(driver);
		productDetailPage.clickOnAddToCartButtonOnProductDetailPage();
	}

	@When("^clicks on ok button provided on alert pop up$")
	public void clicks_on_ok_button_provided_on_alert_pop_up() throws Throwable {
		productDetailPage.productAddedAlert();
	}

	@When("^user moves to checkout from cart page$")
	public void user_moves_to_checkout_from_cart_page() throws Throwable {
		cartPage = new CartPage(driver);
		cartPage.clickCartTab();
		cartPage.placeOrder();
	}

	@When("^user enters Name \"([^\"]*)\" , Country \"([^\"]*)\" , City \"([^\"]*)\" , CreditCard \"([^\"]*)\" , Month \"([^\"]*)\" and Year \"([^\"]*)\"$")
	public void user_enters_Name_Country_City_CreditCard_Month_and_Year(String custName, String custCountry, String custCity, String card, String ccMonth, String ccYear) throws Throwable {
		cartPage.enterCustomerDetails(custName, custCountry, custCity, card, ccMonth, ccYear);
	}
	
	@When("^Purchase the order$")
	public void purchase_the_order() throws Throwable {
		cartPage.clickPurchaseButton();
	}
	
	@Then("^user receives thank you message on checkout page$")
	public void user_receives_thank_you_message_on_checkout_page() throws Throwable {
		checkoutPage = new CheckoutPage(driver);
		checkoutPage.clickOKonThankYouPopAlert();
	}
	
	
	@Given("^clicks on Monitor tab$")
	public void clicks_on_Monitor_tab() throws Throwable {
		homePage.clickMonitorLinkOnHomePage();
	}

	@When("^user selects first Monitor$")
	public void user_selects_first_Monitor() throws Throwable {
		homePage.clickOnFirstMonitor();
	}

	@Given("^clicks on Phone tab$")
	public void clicks_on_Phone_tab() throws Throwable {
		homePage.clickPhoneLinkOnHomePage();
	}

	@When("^user selects first Phone$")
	public void user_selects_first_Phone() throws Throwable {
		homePage.clickOnFirstPhone();
	}

}
*/