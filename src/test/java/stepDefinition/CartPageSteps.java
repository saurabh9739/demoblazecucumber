package test.java.stepDefinition;
import cucumber.api.java.en.When;
import main.java.context.TestContext;
import main.java.pageObjects.CartPage;

public class CartPageSteps {

	TestContext testContext;
	CartPage cartPage;
	
	public CartPageSteps(TestContext testContext)
	{
		this.testContext = testContext;
		cartPage = testContext.getPageObjectManagerInstance().getCartPageInstance();
	}
	
	@When("^user moves to checkout from cart page$")
	public void user_moves_to_checkout_from_cart_page() throws Throwable {
		cartPage.clickCartTab();
		System.out.println("clicked on cart tab");
		cartPage.placeOrder();
		System.out.println("clicked on place order button");
	}

	@When("^user enters Name \"([^\"]*)\" , Country \"([^\"]*)\" , City \"([^\"]*)\" , CreditCard \"([^\"]*)\" , Month \"([^\"]*)\" and Year \"([^\"]*)\"$")
	public void user_enters_Name_Country_City_CreditCard_Month_and_Year(String custName, String custCountry, String custCity, String card, String ccMonth, String ccYear) throws Throwable {
		cartPage.enterCustomerDetails(custName, custCountry, custCity, card, ccMonth, ccYear);
	}
	
	@When("^Purchase the order$")
	public void purchase_the_order() throws Throwable {
		cartPage.clickPurchaseButton();
	}
}
