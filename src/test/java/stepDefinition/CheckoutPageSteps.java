package test.java.stepDefinition;
import cucumber.api.java.en.Then;
import main.java.context.TestContext;
import main.java.pageObjects.CheckoutPage;

public class CheckoutPageSteps {

	TestContext testContext;
	CheckoutPage checkoutPage;
	
	public CheckoutPageSteps(TestContext testContext)
	{
		this.testContext = testContext;
		checkoutPage = testContext.getPageObjectManagerInstance().getCheckoutPageInstance();
	}
	
	@Then("^user receives thank you message on checkout page$")
	public void user_receives_thank_you_message_on_checkout_page() throws Throwable {
		checkoutPage.clickOKonThankYouPopAlert();
	}
}
