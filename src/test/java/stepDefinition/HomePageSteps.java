package test.java.stepDefinition;

import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import main.java.context.TestContext;
import main.java.pageObjects.HomePage;

public class HomePageSteps {

	TestContext testContext;
	HomePage homePage;
	
	public HomePageSteps(TestContext testContext)
	{
		this.testContext = testContext;
		homePage = testContext.getPageObjectManagerInstance().getHomePageInstance();
	}

	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		homePage.navigateToHomePage();
	}

	@Given("^clicks on Laptop tab$")
	public void clicks_on_Laptop_tab() throws Throwable {
		homePage.clickLaptopLinkOnHomePage();
	}

	@When("^user selects first Laptop$")
	public void user_selects_first_Laptop() throws Throwable {
	    homePage.clickOnFirstLaptop();
	}
	
	@Given("^clicks on Monitor tab$")
	public void clicks_on_Monitor_tab() throws Throwable {
		homePage.clickMonitorLinkOnHomePage();
	}

	@When("^user selects first Monitor$")
	public void user_selects_first_Monitor() throws Throwable {
		homePage.clickOnFirstMonitor();
	}

	@Given("^clicks on Phone tab$")
	public void clicks_on_Phone_tab() throws Throwable {
		homePage.clickPhoneLinkOnHomePage();
	}

	@When("^user selects first Phone$")
	public void user_selects_first_Phone() throws Throwable {
		homePage.clickOnFirstPhone();
	}
	
	@Given("^clicks on Login link tab provided on home page$")
	public void clicks_on_Login_link_tab_provided_on_home_page() throws Throwable {
	    homePage.clickLoginLinkOnHomePage();
	}
	
	@When("^user enters valid username and password$")
	public void user_enters_valid_username_and_password(DataTable userCredentials) throws Throwable {
	    List<Map<String, String>> credentials = userCredentials.asMaps(String.class, String.class);
	    homePage.enterUserName(credentials.get(0).get("username"));
	    homePage.enterPassword(credentials.get(0).get("password"));
	    homePage.clickLoginButton();
	}
	
	@Then("^user login to application successfully$")
	public void user_login_to_application_successfully() throws Throwable {
	    System.out.println("login successfully");
	}
	
	@When("^user enters invalid username and invalid password$")
	public void user_enters_invalid_username_and_invalid_password(DataTable userCredentials) throws Throwable {
	    List<List<String>> credentials = userCredentials.raw();
	    homePage.enterUserName(credentials.get(0).get(0));
		homePage.enterPassword(credentials.get(0).get(1));
		homePage.clickLoginButton();
	}

	@Then("^validaion for valid credentials appars on screen$")
	public void validaion_for_valid_credentials_appars_on_screen() throws Throwable {
	    homePage.invalidCredentialsAlertPopUp();
	}

	@When("^user enters null username and null password$")
	public void user_enters_null_username_and_null_password(DataTable userCredentials) throws Throwable {
	    List<List<String>> credentials = userCredentials.raw();
	    homePage.enterUserName(credentials.get(0).get(0));
		homePage.enterPassword(credentials.get(0).get(1));
		homePage.clickLoginButton();
	}

	@When("^user enters valid username and invalid password$")
	public void user_enters_valid_username_and_invalid_password(DataTable userCredentials) throws Throwable {
		List<List<String>> credentials = userCredentials.raw();
		homePage.enterUserName(credentials.get(0).get(0));
		homePage.enterPassword(credentials.get(0).get(1));
		homePage.clickLoginButton();
	}

	@When("^user enters invalid username and valid password$")
	public void user_enters_invalid_username_and_valid_password(DataTable userCredentials) throws Throwable {
		List<List<String>> credentials = userCredentials.raw();
		homePage.enterUserName(credentials.get(0).get(0));
		homePage.enterPassword(credentials.get(0).get(1));
		homePage.clickLoginButton();
	}
}
