package main.java.fileReader;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import main.java.enums.DriverType;

public class ConfigFileReader {

	File file;
	FileReader fileReader;
	Properties properties;
	DriverType driverType;
	
	public ConfigFileReader()
	{
		try {
			file = new File("C:\\Users\\Saura\\eclipse-workspace\\DemoBlazeCucumber\\src\\main\\java\\configs\\config.properties");
			fileReader = new FileReader(file);
			properties = new Properties();
			properties.load(fileReader);
		}catch(Exception e)
		{
			throw new RuntimeException("config.properties does not exists at specified location");
		}
	}
	
	public DriverType getBrowserType()
	{
		String browser = properties.getProperty("browser");
		if(browser.equalsIgnoreCase("chrome"))
		{
			return DriverType.CHROME;
		} 
		else if(browser.equalsIgnoreCase("firefox"))
		{
			return DriverType.FIREFOX;
		}
		else throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browser);
	}
	
	public String getDriverPath()
	{
		String driverPath = properties.getProperty("driverPath");
		if(driverPath != null)
		{
			return driverPath;
		}
		else throw new RuntimeException("Driver path is not specified");
	}
	
	public long getImplicitWait()
	{
		String implicitWait = properties.getProperty("implicitWait");
		if(implicitWait != null)
		{
			return Long.parseLong(implicitWait);
		}
		else throw new RuntimeException("Implicit Wait is not specified");
	}
	
	public String getApplicationUrl()
	{
		String applicationUrl = properties.getProperty("url");
		if(applicationUrl != null)
		{
			return applicationUrl;
		}
		else throw new RuntimeException("Application Url is not specified");
	}
}
