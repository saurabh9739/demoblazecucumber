
package main.java.context;

import main.java.managers.PageObjectManager;
import main.java.managers.WebDriverManager;

public class TestContext {

	WebDriverManager webDriverManager;
	PageObjectManager pageObjectManager;
	
	public TestContext()
	{
		webDriverManager = new WebDriverManager();
		pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
	}
	 
	public WebDriverManager getWebDriverManagerInstance()
	{
		return webDriverManager;
	}
	
	public PageObjectManager getPageObjectManagerInstance()
	{
		return pageObjectManager;
	}
}
