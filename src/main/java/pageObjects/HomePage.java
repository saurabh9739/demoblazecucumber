 package main.java.pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import main.java.managers.FileReaderManager;

public class HomePage {

	WebDriver driver;
	public HomePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);		
	}
	
	@FindBy(how = How.XPATH, using="//*[contains(text(),'Phones')]")
	WebElement PhoneLinkOnHomePage;
	
	@FindBy(how = How.XPATH, using="//*[contains(text(),'Laptops')]")
	WebElement LaptopLinkOnHomePage;
	
	@FindBy(how=How.XPATH, using="//*[contains(text(),'Monitors')]")
	WebElement MonitorLinkOnHomePage;
	
	@FindBy(how= How.XPATH, using="//*[@id='tbodyid']/div/div/a")
	List<WebElement> PhoneListing;
	
	@FindBy(how= How.XPATH, using="//*[@id='tbodyid']/div/div/a")
	List<WebElement> LaptopListing;
	
	@FindBy(how= How.XPATH, using="//*[@id='tbodyid']/div/div/a")
	List<WebElement> MonitorListing;
	
	@FindBy(how=How.XPATH, using="//a[contains(text(),'Log in')]")
	WebElement LoginLinkOnHomePage;
	
	@FindBy(how=How.XPATH, using="//*[@id='loginusername']")
	WebElement userNameTextField;
	
	@FindBy(how=How.XPATH,using="//*[@id='loginpassword']")
	WebElement passwordTextField;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Log in')]")
	WebElement logInButton;
	
	public void navigateToHomePage()
	{
		driver.get(FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getApplicationUrl());
	}
	
	public void clickPhoneLinkOnHomePage()
	{
		PhoneLinkOnHomePage.click();
	} 
	
	public void clickOnFirstPhone()
	{
		for(WebElement element : PhoneListing)
		{
			int count = 0;
			if(count == 0)
			{
				element .click();
				break;
			}
		}
	}
	
	public void clickLaptopLinkOnHomePage()
	{
		LaptopLinkOnHomePage.click();
	}
	
	public void clickOnFirstLaptop()
	{
		for(WebElement element : LaptopListing)
		{
			int count = 0;
			if(count == 0)
			{
				element .click();
				break;
			}
		}
	}
	
	public void clickMonitorLinkOnHomePage()
	{
		MonitorLinkOnHomePage.click();
	}
	
	public void clickOnFirstMonitor()
	{
		for(WebElement element : MonitorListing)
		{
			int count = 0;
			if(count == 0)
			{
				element .click();
				break;
			}
		}
	}
	
	public void clickLoginLinkOnHomePage()
	{
		LoginLinkOnHomePage.click();
	}
	
	public void enterUserName(String username)
	{
		userNameTextField.sendKeys(username);
	}
	
	public void enterPassword(String password)
	{
		passwordTextField.sendKeys(password);
	}
	
	public void clickLoginButton()
	{
		logInButton.click();
	}
	
	public void invalidCredentialsAlertPopUp()
	{
		driver.switchTo().alert().accept();
	}
}
