package main.java.pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetailPage {
	
	WebDriver driver;
	public ProductDetailPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH, using="//*[text()='Add to cart']")
	WebElement addToCartButton;
	
	public void clickOnAddToCartButtonOnProductDetailPage()
	{
		addToCartButton.click();
	}
	
	public void productAddedAlert()
	{
		WebDriverWait explicitiWait = new WebDriverWait(driver, 20);
		explicitiWait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
}
