package main.java.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
	
	WebDriver driver;
	public CartPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH, using="//*[contains(text(),'Cart')]")
	WebElement cartTab;
	
	@FindBy(how=How.XPATH, using="//*[contains(text(),'Place Order')]")
    WebElement placeOrder;
	
	@FindBy(how=How.XPATH, using="//*[@id='name']")
	WebElement name;
	
	@FindBy(how=How.XPATH, using="//*[@id='country']")
	WebElement country;
	
	@FindBy(how=How.XPATH, using="//*[@id='city']")
	WebElement city;
	
	@FindBy(how=How.XPATH, using="//*[@id='card']")
	WebElement creditCard;
	
	@FindBy(how=How.XPATH, using="//*[@id='month']")
	WebElement month;
	
	@FindBy(how=How.XPATH,using="//*[@id='year']")
	WebElement year;
	
	@FindBy(how=How.XPATH, using="//*[contains(text(),'Purchase')]")
	WebElement purchase;
	
	public void clickCartTab()
	{
		cartTab.click();
	}
	
	public void placeOrder()
	{
		placeOrder.click();
	}
	
	public void enterCustomerDetails(String custName, String custCountry, String custCity, String card, String ccMonth, String ccYear)
	{
		name.sendKeys(custName);
		country.sendKeys(custCountry);
		city.sendKeys(custCity);
		creditCard.sendKeys(card);
		month.sendKeys(ccMonth);
		year.sendKeys(ccYear);
	}
	
	public void clickPurchaseButton()
	{
		purchase.click();
	}
	
}
