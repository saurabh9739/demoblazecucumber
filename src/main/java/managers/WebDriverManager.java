package main.java.managers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import main.java.enums.DriverType;

public class WebDriverManager {

	WebDriver driver;
	public WebDriver getDriver()
	{
		DriverType driverType = FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getBrowserType();
		if(driverType.toString().equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getDriverPath());
			ChromeOptions capability = new ChromeOptions();
	        capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	       	capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
	        driver = new ChromeDriver(capability);
	        driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getImplicitWait(), TimeUnit.SECONDS);
		}
		return driver;
	}
	
	public void closeDriver()
	{
		driver.close();
		
	}
}
