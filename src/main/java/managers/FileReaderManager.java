package main.java.managers;

import main.java.fileReader.ConfigFileReader;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager;
	
	public static FileReaderManager getFileReaderManagerInstance()
	{
		return(fileReaderManager == null) ? fileReaderManager = new FileReaderManager() : fileReaderManager;
	}
	
	private ConfigFileReader configFileReader;
	public ConfigFileReader getConfigFileReaderInstance()
	{
		return (configFileReader == null) ? configFileReader = new ConfigFileReader() : configFileReader;
	}
	
}
