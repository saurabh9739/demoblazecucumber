package main.java.hooks;

import cucumber.api.java.After;
import main.java.managers.WebDriverManager;

public class Hooks {
	

	public void beforeScenario()
	{
		
	}
	
	@After
	public void afterScenario()
	{
		WebDriverManager webDriverManager = new WebDriverManager();
		webDriverManager.closeDriver();
	}
}
